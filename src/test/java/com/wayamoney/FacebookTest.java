package com.wayamoney;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FacebookTest {

	
	@Test
	public void Facebook() throws InterruptedException {
		// TODO Auto-generated method stub


		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://facebook.com/");
		driver.manage().window().maximize();
		
		driver.findElement(By.cssSelector("#email")).sendKeys("09166249830");
		Thread.sleep(2000);
		
		driver.findElement(By.cssSelector("#pass")).sendKeys("Tryout456");
		Thread.sleep(2000);
		
		driver.findElement(By.tagName("button")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//span[contains(text(),'Favour Great')]")).click();
		Thread.sleep(2000);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,200)");
		Thread.sleep(4000);
		
		driver.findElement(By.xpath("//span[contains(text(),\"What's on your mind?\")]")).click();
		Thread.sleep(2000);
		
		driver.findElement(By.cssSelector("body._6s5d._71pn.system-fonts--body.segoe:nth-child(2) div.x9f619.x1n2onr6.x1ja2u2z div.__fb-light-mode.x1n2onr6.xzkaem6 div.x9f619.x1n2onr6.x1ja2u2z:nth-child(1) div.x78zum5.xdt5ytf.xg6iff7.x1n2onr6 div.x1uvtmcs.x4k7w5x.x1h91t0o.x1beo9mf.xaigb6o.x12ejxvf.x3igimt.xarpa2k.xedcshv.x1lytzrv.x1t2pt76.x7ja8zs.x1n2onr6.x1qrby5j.x1jfb8zj:nth-child(2) div.__fb-light-mode.x1qjc9v5.x9f619.x78zum5.xdt5ytf.xl56j7k.x1c4vz4f.xg6iff7 div.x1cy8zhl.x9f619.x78zum5.xl56j7k.xeuugli.x47corl.x1sxyh0.xurb0ha.x1x97wu9.xbr3nou.x1dzdb2q.x3v4vwv.x1xoerdy div.x1n2onr6.x1ja2u2z.x1afcbsf.x78zum5.xdt5ytf.x1a2a7pz.x6ikm8r.x10wlt62.x71s49j.x1jx94hy.x1qpq9i9.xdney7k.xu5ydu1.xt3gfkd.x104qc98.x1g2kw80.x16n5opg.xl7ujzl.xhkep3z.x193iq5w div.xt7dq6l.x1a2a7pz.x6ikm8r.x10wlt62.x1n2onr6.x14atkfc:nth-child(1) div.x9f619.x1ja2u2z.x1k90msu.x6o7n8i.x1qfuztq.x10l6tqk.x17qophe.x13vifvy.x1hc1fzr.x71s49j:nth-child(1) div.xh8yej3.x1n2onr6.x5yr21d.x9f619 div.x78zum5.x1q0g3np.xqui1pq.x1pl0jk3.x1plvlek.xryxfnj.x14ocpvf.x5oemz9.x1lck2f0.xlgs127 div.x78zum5.xdt5ytf.x179dxpb.x1pl0jk3.x1n2onr6.xvue9z.x8n7wzh.x11pth41 div.xb57i2i.x1q594ok.x5lxg6s.x6ikm8r.x1ja2u2z.x1pq812k.x1rohswg.xfk6m8.x1yqm8si.xjx87ck.xx8ngbg.xwo3gff.x1n2onr6.x1oyok0e.x1odjw0f.x1e4zzel.x78zum5.xdt5ytf.x1iyjqo2:nth-child(2) div.x78zum5.xdt5ytf.x1iyjqo2.x1n2onr6 div.x1ed109x.x1iyjqo2.x5yr21d.x1n2onr6.xh8yej3 div.x9f619.x1iyjqo2.xg7h5cd.x1pi30zi.x1swvt13.x1n2onr6.xh8yej3.x1ja2u2z.x1t1ogtf:nth-child(1) div.xl56j7k.x78zum5 div.x76ihet.xwmqs3e.x112ta8.xxxdfa6.x9f619.xzsf02u.xmper1u.xngnso2.xo1l8bm.x5yr21d.x1qb5hxa.x1a2a7pz.x1iorvi4.x4uap5.xwib8y2.xkhd6sd.xh8yej3.xha3pab div.xzsf02u.x1a2a7pz.x1n2onr6.x14wi4xw.x9f619.x1lliihq.x5yr21d.xh8yej3.notranslate > p.x16tdsg8.x1mh8g0r.xat24cr.x11i5rnm.xdj266r")).sendKeys("Hello World");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]")).click();
		Thread.sleep(2000);
		
		driver.close();
		
		
	}

}
